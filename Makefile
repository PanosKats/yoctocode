TARGETS = panos

SOURCE_PATH = src/src

SOURCES = PanosTest.c printer.c

HEADERS = printer.h

OBJECTS = PanosTest.o printer.o

ifeq (.depend,$(wildcard .depend))
include .depend
endif

INCLUDE_DIRS = -I./src

INSTALL = install

all: $(TARGETS)

%.o: $(SOURCE_PATH)/%.c
	$(CC) -c $(CFLAGS) $(INCLUDE_DIRS) -o $@ $<

panos: $(OBJECTS)
	$(CC) -o $@ $^

clean:
	rm -f $(OBJECTS) $(TARGETS) *~ #* core

depend:
	$(CPP) -M $(CFLAGS) $(SOURCES) >.depend

