/*
 ============================================================================
 Name        : PanosTest.c
 Author      : Panos
 Version     :
 Copyright   : No copyright
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "inc/printer.h"

int main(void) {
	printMessage("Hello World\n");
	return EXIT_SUCCESS;
}
