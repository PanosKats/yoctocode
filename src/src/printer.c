/*
 ============================================================================
 Name        : printer.c
 Author      : Panos
 Version     :
 Copyright   : No copyright
 Description : Print help functions
 ============================================================================
 */

#include <stdio.h>
#include "inc/printer.h"

void printMessage(char* msg) {
	printf("%s", msg);
}
